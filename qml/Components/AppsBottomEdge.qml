import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

import Sprint 1.0

BottomEdge {
    id: bottomEdge

    signal openSettings()
    signal showApp(var app)

    hint.text: i18n.tr("Apps")
    preloadContent: false

    Timer {
        interval: 1
        repeat: false
        running: true
        onTriggered: bottomEdge.preloadContent = true
    }

    contentComponent: Rectangle {
        id: bottomEdgeComponent

        width: bottomEdge.width
        height: bottomEdge.height

        property bool searching: false

        PageHeader {
            id: bottomEditHeader
            title: i18n.tr('Apps')

            trailingActionBar {
                actions: [
                    Action {
                        text: i18n.tr('Settings')
                        iconName: 'settings'

                        onTriggered: {
                            bottomEdge.collapse();
                            bottomEdge.openSettings();
                        }
                    },
                    Action {
                        text: i18n.tr('Search')
                        iconName: 'search'

                        onTriggered: {
                            searching = true;
                            searchField.forceActiveFocus();
                        }
                    }
                ]
            }
        }

        Rectangle {
            visible: searching

            anchors.fill: bottomEditHeader

            RowLayout {
                anchors {
                    fill: parent
                    margins: units.gu(1)
                }
                spacing: units.gu(1)

                Icon {
                    Layout.fillHeight: true
                    Layout.preferredWidth: height
                    Layout.topMargin: units.gu(0.5)
                    Layout.bottomMargin: units.gu(0.5)

                    name: 'back'

                    MouseArea {
                        anchors.fill: parent

                        onClicked: searchField.focus = false
                    }
                }

                TextField {
                    id: searchField

                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.rightMargin: units.gu(1)

                    inputMethodHints: Qt.ImhNoPredictiveText
                    placeholderText: i18n.tr('Search apps...')

                    onActiveFocusChanged: {
                        if (!activeFocus) {
                            searchField.text = '';
                            searching = false;
                            Application.clearFilter();
                        }
                    }

                    onTextChanged: Applications.filter(text)
                }
            }
        }

        Flickable {
            anchors {
                top: bottomEditHeader.bottom
                right: parent.right
                left: parent.left
                bottom: parent.bottom
            }

            clip: true
            contentHeight: grid.height + units.gu(5)

            /*
            property bool close: false

            onContentYChanged: {
                if (contentY > pullToClose.y) {
                    close = true;
                }

                if (contentY >= 0 && close) {
                    close = false;
                    bottomEdge.collapse();
                }
            }

            Label {
                id: pullToClose

                anchors {
                    left: parent.left
                    right: parent.right
                    bottom: grid.top
                    bottomMargin: units.gu(3)
                }

                text: i18n.tr('Pull to close')
                horizontalAlignment: Text.AlignHCenter
            }
            */

            GridLayout {
                id: grid

                anchors {
                    top: parent.top
                    right: parent.right
                    left: parent.left
                    margins: units.gu(1)
                }

                columns: 4
                columnSpacing: units.gu(1)
                rowSpacing: units.gu(1)

                Repeater {
                    model: Applications.filteredList

                    delegate: Launcher {
                        Layout.fillWidth: true
                        Layout.preferredHeight: width + units.gu(2)
                        Layout.maximumWidth: appWidth

                        app: model
                        color: 'black'

                        onLaunch: bottomEdge.collapse()
                        onPressAndHold: {
                            bottomEdge.collapse();
                            bottomEdge.showApp(model);
                        }
                    }
                }
            }
        }
    }
}
