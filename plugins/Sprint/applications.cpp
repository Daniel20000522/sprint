#include <algorithm>

#include <QDebug>
#include <QDirIterator>

#include "applications.h"
#include "app.h"

bool compareApp(App *a, App *b) {
    return (a->name().compare(b->name(), Qt::CaseInsensitive) < 0);
}

Applications::Applications() {
    connect(&m_uninstallProcess, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(uninstallProcessFinished(int, QProcess::ExitStatus)));

    refresh();
}

void Applications::refresh() {
    m_applications.clear();

    loadDesktopFiles(DESKTOP_FILES_FOLDER_SYSTEM);
    loadDesktopFiles(DESKTOP_FILES_FOLDER_USER);

    std::sort(m_applications.begin(), m_applications.end(), compareApp);

    Q_EMIT listChanged();

    filter(QStringLiteral(""));
    loadApps(true);
    loadApps(false);
    
    m_appsLoaded = true;
    if (m_numLauncher && m_numFavorites) {
        initialize(m_numLauncher, m_numFavorites);
    }
}

void Applications::filter(const QString &searchTerm) {
    m_searchTerm = searchTerm;
    m_filteredApplications.clear();

    Q_FOREACH(App *app, m_applications) {
        // TODO search keywords
        if (app->name().contains(m_searchTerm, Qt::CaseInsensitive) || m_searchTerm.isEmpty()) {
            m_filteredApplications.append(app);
        }
    }

    Q_EMIT filteredListChanged();
}

void Applications::clearFilter() {
    filter(QStringLiteral(""));
}

App* Applications::get(const QString &appId) {
    App *found = nullptr;

    Q_FOREACH(App *app, m_applications) {
        if (app->appId() == appId || app->id() == appId) {
            found = app;
            break;
        }
    }

    return found;
}

void Applications::loadDesktopFiles(const QString &path) {
    QDirIterator dir(path, QDir::Files | QDir::NoDotAndDotDot | QDir::Readable, QDirIterator::Subdirectories);
    QRegExp rx = QRegExp(QStringLiteral("*.desktop"), Qt::CaseInsensitive);
    rx.setPatternSyntax(QRegExp::Wildcard);

    while (dir.hasNext()) {
        dir.next();
        if (rx.exactMatch(dir.fileName())) {
            QString desktopFile = dir.fileInfo().absoluteFilePath();
            App* app = new App(desktopFile);

            if (app->isVisible() && app->id() != QStringLiteral("com.ubuntu.sprint_sprint")) {
                m_applications.append(app);
            }
            else {
                app->deleteLater();
            }
        }
    }
}

QQmlListProperty<App> Applications::list() {
    return QQmlListProperty<App>(this, m_applications);
}

QQmlListProperty<App> Applications::filteredList() {
    return QQmlListProperty<App>(this, m_filteredApplications);
}

QQmlListProperty<App> Applications::launcherList() {
    return QQmlListProperty<App>(this, m_launcherApplications);
}

QQmlListProperty<App> Applications::favoriteList() {
    return QQmlListProperty<App>(this, m_favoriteApplications);
}

bool Applications::isLauncherEmpty() const {
    Q_FOREACH(App *app, m_launcherApplications) {
        if (!!app) {
            return false;
        }
    }

    return true;
}

void Applications::uninstall(const QString &packageName, const QString &version) {
    QStringList args;
    args << QStringLiteral("remove") << packageName + QStringLiteral(";") + version + QStringLiteral(";all;local:click");

    qDebug() << args;

    m_uninstallProcess.start("pkcon", args);
}

void Applications::uninstallProcessFinished(int exitCode, QProcess::ExitStatus exitStatus) {
    qDebug() << "uninstalling finished" << exitCode << exitStatus;
    qDebug() << "stdout:" << m_uninstallProcess.readAll();

    bool ok = (exitCode == 0 && exitStatus == 0);

    Q_EMIT uninstallFinished(ok);
    if (ok) {
        refresh();
    }
}


void Applications::loadApps(const bool launcher) {
    QString key;
    if (launcher) {
        key = QStringLiteral("launcherApplications");
        m_launcherApplications.clear();
    }
    else {
        key = QStringLiteral("favoriteApplications");
        m_favoriteApplications.clear();
    }

    QStringList packageNames = m_settings.value(key).toStringList();
    if (!launcher && packageNames.isEmpty()) {
        packageNames << "dialer-app" << "messaging-app" << "morph-browser" << "com.ubuntu.camera_camera";
    }
    
    Q_FOREACH(QString id, packageNames) {
        App *app = nullptr;
        if (!id.isEmpty()) {
            app = get(id);
        }
        
        if (launcher) {
            m_launcherApplications.append(app);
        }
        else {
            m_favoriteApplications.append(app);
        }
    }
    
    if (launcher) {
        Q_EMIT launcherListChanged();
    }
    else {
        Q_EMIT favoriteListChanged();
    }
}

void Applications::initialize(const int numLauncher, const int numFavorites) {
    m_numLauncher = numLauncher;
    m_numFavorites = numFavorites;

    if (m_appsLoaded) {
        // TODO handle more than the limits
        
        while (m_launcherApplications.count() < m_numLauncher) {
            m_launcherApplications.append(nullptr);
        }
        
        while (m_favoriteApplications.count() < m_numFavorites) {
            m_favoriteApplications.append(nullptr);
        }
        
        Q_EMIT launcherListChanged();
        Q_EMIT favoriteListChanged();
    }
}

void Applications::insertLauncherApp(const int index, const QString appId) {
    if (index < 0 || index > m_numLauncher) {
        return;
    }
    
    m_launcherApplications.replace(index, get(appId));
    
    QStringList ids;
    Q_FOREACH(App *app, m_launcherApplications) {
        if (app) {
            ids << app->id();
        }
        else {
            ids << QStringLiteral("");
        }
    }
    
    m_settings.setValue(QStringLiteral("launcherApplications"), QVariant(ids));
    m_settings.sync();
    
    Q_EMIT launcherListChanged();
}

void Applications::insertFavoriteApp(const int index, const QString appId) {
    if (index < 0 || index > m_numFavorites) {
        return;
    }
    
    m_favoriteApplications.replace(index, get(appId));
    
    QStringList ids;
    Q_FOREACH(App *app, m_favoriteApplications) {
        if (app) {
            ids << app->id();
        }
        else {
            ids << QStringLiteral("");
        }
    }
    
    m_settings.setValue(QStringLiteral("favoriteApplications"), QVariant(ids));
    m_settings.sync();
    
    Q_EMIT favoriteListChanged();
}
